import { getElWithId, currentProduct } from "./index.js";

/***    BANKER ELEMENTS   ***/
const bankBalance = getElWithId("bank");
const loanBalance = getElWithId("loan");
const loanButton = getElWithId("loanButton");
const loanContainer = document.getElementsByClassName("loan")[0];

/***   WORK ELEMENTS   ***/
const income = getElWithId("income");
const transferButton = getElWithId("toBankButton");
const workButton = getElWithId("doWorkButton");
const repayButton = getElWithId("repayButton");

/***  PRODUCT ELEMENT  ***/
const buyButton = getElWithId("pcBuyButton");

// Global variables
export const currency = {
    bank: 0,
    loan: 0,
    pay: 0,
    bought: true
};

// Add x money to work counter and update element
workButton.addEventListener("click", () => {
    currency.pay += 100;
    updateCurrencyDisplay();
});

// transfer money over to bank, take the 10% from work and substract it on the loan, udpate element
transferButton.addEventListener("click", () => {
    let c = currency;
    const toDeduct = c.pay / 10;
    if(c.loan > 0) {
        c.loan -= toDeduct;
        if(c.loan < 0) c.loan = 0;
        c.pay = c.pay - toDeduct;
    }
    c.bank += c.pay;
    c.pay = 0;
    updateCurrencyDisplay();
});

// Add all pay that can go to paying down the loan
repayButton.addEventListener("click", () => {
    let c = currency;
    
    // if pay is more than loan, substract and return rest to bank, update loan status and element
    if(c.pay >= c.loan) {
        c.bank += c.pay - c.loan;
        c.loan = 0;
        c.pay = 0;
        updateCurrencyDisplay();
        return;
    }
    c.loan -= c.pay;
    c.pay = 0;
    updateCurrencyDisplay();
});

// Only when button isnt disabled, check the input value and give loan if valid number
loanButton.addEventListener("click", () => {
    if(!currency.bought) {
        alert("You must buy a computer first before taking another loan!");
        return;
    }
    let c = currency;
    const attemptedLoan = parseInt( prompt("How much money do you need?") );
    if(!attemptedLoan) return;
    // Check if input is higher than twice the amount in bank 
    if(attemptedLoan > (c.bank * 2)) { 
        alert(`You cannot loan more than twice your bank value. Max is ${c.bank * 2}`);
        return;
    }
    // If input is zero or negative value
    if(attemptedLoan <= 0 ) {
        alert("This isnt even possible");
        return;
    }
    // Input is valid
    c.loan = attemptedLoan;
    c.bought = false;
    updateCurrencyDisplay();
});

// Buy the current laptop
buyButton.addEventListener("click", () => {
    if(currentProduct.price > currency.bank) {
        alert("You do not have enough money for this laptop")
        return;
    }
    currency.bank -= currentProduct.price;
    alert(`You just bought ${currentProduct.title} for ${currentProduct.price}!`);
    currency.bought = true;
    updateCurrencyDisplay();
});

// Updates UI with the new values
function updateCurrencyDisplay() {
    checkLoan();
    bankBalance.innerText = `${currency.bank},- kr`;
    loanBalance.innerText = `${currency.loan},- kr`;
    income.innerText = `${currency.pay},- kr`;
}

// Logic for enable and disable button according to bank and loan
function checkLoan() {
    if(currency.loan > 0) {
        loanButton.disabled = false;
        repayButton.classList.remove("hide");
        loanContainer.classList.remove("hide");
    }
    if(currency.loan === 0 && currency.bank !== 0) {
        repayButton.classList.add("hide");
        loanButton.disabled = false;
    }
    if(currency.bank === 0) {
        loanContainer.classList.add("hide");
        repayButton.classList.add("hide");
        loanButton.disabled = true;
    }
}

updateCurrencyDisplay();