# Assignment 1 - Komputer Store
This assignment requires no npm or node to be pre-installed

### File structures
For files the seperation of logic handling has been done.

index.js: Handles all logic from fetching data from the external API through the api.js module and assigns global variable to store and render correct laptop according to dropdown list.

economy.js: Handles all logic according to the money transferring, working and loans. Also handles what element and button is shown or disabled according to requirements.

api.js: Fetch call to external API to get the array of laptops to show in the "shop"

### Other
If more time were to be used or more precision, the next step would be to properly update the meta tags in the html to better SEO requirements, better screen reader attributes and Open Graph implementation.

### Funfact
No funfact since we cant custom make the laptop :x