/*
    Even though this file only contains one method it is purely meant for seggregation of functionality within the application.
    Since we dont want to handle all of the logic in one file so that api.js can handle the possible errors
*/

// Endpoint to get data no matter the endpoint provided
async function getData(endpoint) {
    try {
        return await fetch(endpoint).then(response => response.json());
    } catch (error) {
        console.error("Something went wrong in fetching data", error);
    }
}

export default getData;