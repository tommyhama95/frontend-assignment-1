import getData from "./api.js";


// Arrow function to use the getElementById() method in document. 
// Used to ensure no typos when getting element 
export const getElWithId = id => document.getElementById(id);


/***   LAPTOPS ELEMENTS   ***/
const dropdownMenu = getElWithId("dropdownMenu");
const pcDetails = getElWithId("pcDetails");

/***   PRODUCT ELEMENTS   ***/
const card = document.getElementsByClassName("laptopcard")[0];
const imagePC = getElWithId("pcImage");
const titlePC = getElWithId("pcName");
const descriptionPC = getElWithId("pcDescription");
const pricePC = getElWithId("price");


// Global variables 
const DEFAULT_ENDPOINT = "https://noroff-komputer-store-api.herokuapp.com/";
const LAPTOPS_ENDPOINT = "computers";
let laptops;
export let currentProduct;


// get all laptops and feed the necessary data for the webpage on page
async function loadLaptops() {
    laptops = await getData(DEFAULT_ENDPOINT + LAPTOPS_ENDPOINT);
    // remove default data if no connection
    dropdownMenu.innerHTML = "";
    card.classList.remove("hide");
    updateDropDown(laptops);
    // default display the first laptop
    currentProduct = laptops[0];
    displayLaptop(laptops[0]);
}

// Updates Select element with all laptops as an option element
function updateDropDown(laptops) {
    laptops.forEach(laptop => {
        const elOption = document.createElement("option");
        elOption.innerText = laptop.title;
        elOption.setAttribute("value", laptop.id);
        dropdownMenu.appendChild(elOption);
    });
}

// Get the new id selected and show that laptop
dropdownMenu.addEventListener("change", (e) => {
    let product = laptops.find(laptop => laptop.id === parseInt(e.target.value));
    currentProduct = product;
    displayLaptop(product);
});

// Display a laptop as the product
async function displayLaptop(laptop) {
    const { title, description, price, image, specs }  = laptop;

    // Filling elements with value of laptop
    imagePC.setAttribute("src", `${DEFAULT_ENDPOINT}${image}`);
    imagePC.setAttribute("alt", `${title} - Laptop`);
    titlePC.innerText = title;
    descriptionPC.innerText = description;
    pricePC.innerText = `${price} NOK`;

    pcDetails.innerHTML = "";
    specs.forEach(spec => {
        const li = document.createElement("li");
        li.innerText = spec;
        pcDetails.appendChild(li);
    });
}

// Set new default src to image if it gets an error
imagePC.addEventListener("error", () => {
    imagePC.setAttribute("src", "https://help4access.com/wp-content/uploads/2020/12/no-image-available.jpeg");
});

loadLaptops();